export class RegisterUserInfo {
    username: string;
    password: string;
    confirmPassword: string;
}